import Meals from "../model/Meals";
import Meal from "../model/Meal";
import Grade from "../model/Grade";

export default [
  new Meals(Grade.MauGiao, [
    new Meal("Bữa Sáng", "07:45", ["Miến lươn", "Sữa tươi TH True Milk", "Hoa quả"], "https://images.unsplash.com/photo-1550547660-d9450f859349?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Zm9vZCUyMGltYWdlfGVufDB8fDB8fA%3D%3D&w=1000&q=80"),
    new Meal("Bữa Trưa", "10:30", ["Cơm trắng", "Cá diêu Hồng", "Canh rau má", "Đậu bắp luộc", "Dưa leo", "Dưa hấu"], "https://images.unsplash.com/photo-1546069901-ba9599a7e63c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHx8&w=1000&q=80"),
    new Meal("Bữa Chiều", "14:00", ["Bún thang", "Sữa tươi TH True Milk"], "https://filebroker-cdn.lazada.vn/kf/S2a2dba8ea8c2462ea621c52989834488P.jpg"),
  ]),
  new Meals(Grade.NhaTre, [
    new Meal("Bữa Sáng", "07:45", ["Miến lươn", "Sữa tươi TH True Milk", "Hoa quả"], "https://images.unsplash.com/photo-1550547660-d9450f859349?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Zm9vZCUyMGltYWdlfGVufDB8fDB8fA%3D%3D&w=1000&q=80"),
    new Meal("Bữa Trưa", "10:30", ["Cơm trắng", "Cá diêu Hồng", "Canh rau má", "Đậu bắp luộc", "Dưa leo", "Dưa hấu"], "https://images.unsplash.com/photo-1546069901-ba9599a7e63c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHx8&w=1000&q=80"),
  ]),
]