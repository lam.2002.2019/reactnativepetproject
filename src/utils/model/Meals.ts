import Grade from "./Grade";
import Meal from "./Meal";

export default class Meals {
  gradeName: Grade;
  meals: Meal[];

  constructor(gradeName: Grade, meals: Meal[]) {
    this.gradeName = gradeName;
    this.meals = meals;
  }
}