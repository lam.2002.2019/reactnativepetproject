export default class Meal {
  name: string
  private _time: Date
  foods: string[]
  image: string

  constructor(name: string, time: string, foods: string[], image: string) {
    this.name = name
    this._time = this.timeStringToDateTime(time)
    this.foods = foods
    this.image = image
  }

  get time(): string {
    return this._time.toTimeString().slice(0, 5)
  }

  set time(time: string) {
    this._time = this.timeStringToDateTime(time)
  }

  get timeInDate(): Date {
    return this._time
  }

  private timeStringToDateTime(time: string): Date {
    if (!/^\d{2}:\d{2}$/.test(time)) {
      throw new Error('Invalid time format')
    }
    const [hours, minutes] = time.split(':')
    const date = new Date()
    date.setHours(Number(hours))
    date.setMinutes(Number(minutes))
    return date
  }

}