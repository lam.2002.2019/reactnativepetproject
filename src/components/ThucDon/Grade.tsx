import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Meals from '../../utils/model/Meals';
import Meal from './Meal';

type Props = {
  meals: Meals,
  containerStyle?: any,
};

export default function Grade({ meals, containerStyle }: Props) {
  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.gradeName}>{meals.gradeName}</Text>
      <View>
        {meals.meals.map(meal => (
          <Meal meal={meal} key={meal.name} containerStyle={styles.meal} />
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  gradeName: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  meal: {
    marginBottom: 10,
  },
});