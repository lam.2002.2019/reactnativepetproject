import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import MealModel from '../../utils/model/Meal';
import TimeLabel from '../common/TimeLabel';
import LongList from '../common/LongList';
import TextButton from '../common/TextButton';

type Props = {
  meal: MealModel,
  containerStyle?: any,
};

const MAX_ITEM = 4

export default function Meal({ meal, containerStyle }: Props) {
  const [isShowAll, setIsShowAll] = React.useState(false)
  const shouldShowMore = React.useMemo(() => meal.foods.length > MAX_ITEM, [meal.foods.length])

  return (
    <View style={[styles.container, containerStyle]}>
      <Image source={{uri: meal.image}} style={styles.image} />
      <View style={styles.content}>
        <View style={styles.contentHeader}>
          <Text style={styles.MealName}>{meal.name}</Text>
          <TimeLabel time={meal.time} />
        </View>
        <View style={styles.foodContainer}>
          <LongList
            list={meal.foods.map((food, i) => `${i + 1}. ${food}`)}
            max={MAX_ITEM}
            customStyle={styles.foodList}
            showAll={isShowAll}
          />
          {shouldShowMore &&
            <TextButton text={isShowAll ? "Thu gọn" : "Xem thêm"} onPress={() => setIsShowAll(!isShowAll)} />}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  image: {
    width: 110,
    height: 110,
    borderRadius: 5,
  },
  content: {
    paddingLeft: 10,
    flex: 1,
  },
  contentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  MealName: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  TimeContainer: {},
  Time: {},
  foodContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  foodList: {
    flex: 1,
  },
  food: {
    
  },
  toggleShowText: {
    fontSize: 10,
    textDecorationLine: 'underline',
    color: 'blue',
  },
});