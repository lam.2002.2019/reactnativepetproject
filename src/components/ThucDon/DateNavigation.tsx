import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

type Props = {};

export default function DateNavigation({}: Props) {
  return (
    <View style={styles.container}>
      <Icon name="chevron-left" size={20} />
      <Text>Hôm nay | 14/4</Text>
      <Icon name="chevron-right" size={20} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 10,
    padding: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },
});