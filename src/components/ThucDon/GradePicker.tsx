import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'
import Icon from 'react-native-vector-icons/FontAwesome'
import Grade from '../../utils/model/Grade'

export enum GradeOptionAll { All = "Tất cả" }
export type GradeOptionType = Grade | GradeOptionAll
const GradeOption = { ...GradeOptionAll, ...Grade }
type Props = {
  grade: GradeOptionType,
  setGrade: Function
};

export default function GradePicker({ grade, setGrade }: Props) {
  return (
    <View>
      <Text style={styles.label}>Khối</Text>
      <SelectDropdown
        data={Object.values(GradeOption)}
        onSelect={selectedItem => setGrade(selectedItem)}
        defaultValue={grade}
        renderDropdownIcon={() => {
          return(<Icon name="chevron-down" size={20} />)
        }}
        buttonStyle={styles.buttonStyle}
        buttonTextStyle={styles.buttonTextStyle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  label: {
    fontWeight: 'bold'
  },
  buttonStyle: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 3,
    height: 35,
  },
  buttonTextStyle: {
  }
});