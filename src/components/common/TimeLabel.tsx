import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

type Props = {
  time: string
};

export default function TimeLabel({ time }: Props) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{time}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fcfaec',
    borderRadius: 5,
  },
  text: {
    padding: 2,
    fontWeight: 'bold',
    color: '#a78c53',
  }
});