import React from 'react';
import { View, StyleSheet, Pressable, Text } from 'react-native';

type Props = {
  text: string,
  onPress?: () => void
};

export default function TextButton({ text, onPress }: Props) {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    fontSize: 10,
    textDecorationLine: 'underline',
    color: 'blue',
  },
});