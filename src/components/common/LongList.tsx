import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

type Props = {
  list: string[],
  max?: number,
  showAll?: boolean,
  customStyle?: any
};

export default function LongList({ list, max, showAll, customStyle }: Props) {
  let maxItem = max || 4
  let visibleList = list.slice(0, maxItem)
  let hiddenList = list.slice(maxItem)
  let isShowAll = showAll || false
  return (
    <View style={[styles.container, customStyle]}>
      {visibleList.map((item, index) => (
        <Text key={index} style={styles.text}>{item}</Text>
      ))}
      {isShowAll && hiddenList.map((item, index) => (
        <Text key={index} style={styles.text}>{item}</Text>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    fontSize: 12,
  }
});