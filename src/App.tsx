import React from "react";
import TabBarScreen from "./view/TabBarScreen";
import { NavigationContainer } from "@react-navigation/native";

export default function App() {
  return (
    <NavigationContainer>
      <TabBarScreen />
    </NavigationContainer>
  );
}
