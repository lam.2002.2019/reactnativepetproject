import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ThucDon from "./ThucDon";
import MainScreen from "./MainScreen";
import ThucDonSelect from "./ThucDonSelect";
import ImageSelect from "./ImageSelect";

type Props = {};

export type RootStackParamList = {
  MainScreen: undefined;
  ThucDon: undefined;
  ThucDonSelect: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();

export default function Home({}: Props) {
  return (
    <Stack.Navigator
      initialRouteName="MainScreen"
      screenOptions={{
        animation: "default",
        headerShadowVisible: false,
        headerStyle: {
          backgroundColor: "#fff",
        },
        headerTitleAlign: "center",
      }}
    >
      <Stack.Screen
        name="MainScreen"
        component={MainScreen}
        options={{ title: "Home" }}
      />
      <Stack.Screen
        name="ThucDon"
        component={ThucDon}
        options={{ title: "Thực đơn" }}
      />
      <Stack.Screen
        name="ThucDonSelect"
        component={ThucDonSelect}
        options={{ title: "" }}
      />
      
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {},
});
