import React, { useState, useMemo } from 'react';
import { View, StyleSheet, Text, ScrollView, Button } from 'react-native';
import GradePicker, { GradeOptionAll, GradeOptionType } from '../../components/ThucDon/GradePicker';
import Meals from '../../utils/model/Meals';
import FakeData from '../../utils/fakeData/Meal'
import Grade from '../../components/ThucDonSelect/Grade';
import Meal from '../../utils/model/Meal';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

type Props = {};

export default function ThucDonSelect({}: Props) {
  const [grade, setGrade] = useState<GradeOptionType>(GradeOptionAll.All)
  const [meals, setMeals] = useState<Meals[]>(FakeData)
  const [selectedMeals, setSelectedMeals] = useState<Meal[]>([])
  
  function handleSelectMeal(meal: Meal) {
    if (selectedMeals.includes(meal)) {
      setSelectedMeals(selectedMeals.filter(selectedMeal => selectedMeal !== meal))
    } else {
      setSelectedMeals([...selectedMeals, meal])
    }
  }

  const filteredMeals = useMemo(() => {
    if (grade === GradeOptionAll.All) return meals
    return meals.filter(meal => meal.gradeName === grade)
  }, [grade, meals])

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Chọn những bữa ăn có cùng thực đơn</Text>
      <ScrollView>
        <View style={styles.gradeContainer}>
          <Text>{`Đã chọn ${selectedMeals.length} bữa`}</Text>
          <GradePicker grade={grade} setGrade={setGrade} />
        </View>
        <View>
          {filteredMeals.map(meals => (
            <Grade
              meals={meals}
              key={meals.gradeName}
              containerStyle={styles.grade}
              selectedMeals={selectedMeals}
              onSelectMeal={handleSelectMeal}
            />
          ))}
        </View>
      </ScrollView>
      <View style={styles.continueButton}>
        <Button title="Tiếp tục" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10,
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 10,
  },
  gradeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10
  },
  grade: {
    marginBottom: 10,
  },
  continueButton: {
    marginTop: 10,
  }
});