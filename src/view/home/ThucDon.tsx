import React, { useState, useMemo } from 'react';
import { View, StyleSheet, Text, Button, ScrollView } from 'react-native';
import DateNavigation from '../../components/ThucDon/DateNavigation';
import GradePicker, { GradeOptionType, GradeOptionAll } from '../../components/ThucDon/GradePicker';
import Meals from '../../utils/model/Meals';
import FakeData from '../../utils/fakeData/Meal'
import Grade from '../../components/ThucDon/Grade';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootStackParamList } from './Home';

type Props = {
  navigation: NativeStackNavigationProp<RootStackParamList, "MainScreen">;
};

export default function ThucDon({ navigation }: Props) {
  const [grade, setGrade] = useState<GradeOptionType>(GradeOptionAll.All)
  const [meals, setMeals] = useState<Meals[]>(FakeData)

  const filteredMeals = useMemo(() => {
    if (grade === GradeOptionAll.All) return meals
    return meals.filter(meal => meal.gradeName === grade)
  }, [grade, meals])

  return (
    <View style={styles.container}>
      <DateNavigation />
      <ScrollView style={styles.detail}>
        <Button title="Tải ảnh lên" onPress={() => navigation.navigate('ThucDonSelect')} />
        <View style={styles.gradeContainer}>
          <GradePicker grade={grade} setGrade={setGrade} />
        </View>
        <View>
          {filteredMeals.map(meals => (
            <Grade meals={meals} key={meals.gradeName} containerStyle={styles.grade} />
          ))}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  detail: {
    padding: 10,
  },
  gradeContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 10
  },
  grade: {
    marginBottom: 10,
  }
});