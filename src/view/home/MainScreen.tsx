import React from "react";
import { View, StyleSheet, Button } from "react-native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { RootStackParamList } from "./Home";

type Props = {
  navigation: NativeStackNavigationProp<RootStackParamList, "MainScreen">;
};

export default function Home({ navigation }: Props) {
  return (
    <View style={styles.container}>
      <Button
        title="Open Thực Đơn"
        onPress={() => navigation.navigate("ThucDon")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 50,
  },
});
